'use strict';
const { add } = require('./add/index.js') 
const { get } = require('./get/index.js')
// 这里感觉应该 引个文件 用枚举
// 因为是node 就直接用global全局对象
global.successMsg = 'success' 
global.successCode = 0 
global.wrongCode = -1

exports.main = async (event, context) => {
	switch (event.type) { 
		case 'add':
			return add(event) 
		case 'get':
			return get(event) 
	}
};
