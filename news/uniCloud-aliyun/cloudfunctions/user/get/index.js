const db = uniCloud.database()

exports.get = async (event) => {
  	const collection = db.collection('user')
  	let user = await collection.where({
		username: event.username,
		password: event.password
	}).get()

	if (user.affectedDocs < 1) {
		return {
			code: global.wrongCode,
			msg: '用户名或密码错误'
		}
	} else {
		return {
			code: global.successCode,
			msg: global.successMsg
		}
	}
};
